<?php
namespace MyParcelAsia\MyParcelAsia\Block\Adminhtml;

use MyParcelAsia\MyParcelAsia\Helper\Data;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\Order;

use Magento\Customer\Model\Session;

 
class GetCountry extends Template
{
	public function __construct(Context $context, Data $helper, Order $orderRepository)
    {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function getCountry() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->helper->getUrlCountry(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$this->helper->getApiKey()."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);

        // if ($err) {
        //   echo "cURL Error #:" . $err;
        // } else {
        //   echo '<pre>';
        //   print_r($response);
        //   echo '</pre>';
        // }
    }
}