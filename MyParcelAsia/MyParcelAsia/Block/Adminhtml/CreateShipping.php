<?php
namespace MyParcelAsia\MyParcelAsia\Block\Adminhtml;
 
class CreateShipping extends \Magento\Framework\View\Element\Template
{
    public function __construct(
        \Magento\Store\Model\StoreManagerInterface $storeManager, 
        \MyParcelAsia\MyParcelAsia\Helper\Data $helper, 
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\App\Response\Http $response, 
        \Magento\Framework\Data\Form\FormKey $formKey,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Sales\Model\Order $order,
        array $data = []
    )
    {
        $this->_storeManager = $storeManager;
        $this->helper = $helper;
        $this->response = $response;
        $this->formKey = $formKey;
        $this->coreSession = $coreSession;
        $this->_order = $order;
        parent::__construct($context, $data);
    }

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl();
    }

    /**
     * get form key
     *
     * @return string
     */
    public function getFormKey()
    {
         return $this->formKey->getFormKey();
    }

    public function getStoreAddress()
    {

    }

    public function getParcelInfo() {
        $data = $this->coreSession->getData('mpa_createshipping');
        return $data;
    }

    public function getQuotesInfo()
    {
        $data = $this->coreSession->getData('mpa_price');
        
        $country = explode('-', $data['country_id']);
        $data['country_code'] = $country[0];
        $data['country_name'] = $country[1];
        return $data;
    }

    public function getSenderInfo() {
        $data['name'] = $this->helper->getSenderName();
        $data['email'] = $this->helper->getSenderEmail();
        $data['phone'] = $this->helper->getSenderContact1();
        $data['add1'] = $this->helper->getSenderAddress1();
        $data['add2'] = $this->helper->getSenderAddress2();
        $data['city'] = $this->helper->getSenderCity();
        $data['postcode'] = $this->helper->getSenderPostcode();
        $data['state'] = $this->helper->getSenderState();
        $data['country'] = $this->helper->getSenderCountry();
        return $data;
    }

    public function getStates() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->helper->getUrlStates(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$this->helper->getApiKey()."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);
    }

    public function getCountry() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->helper->getUrlCountry(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$this->helper->getApiKey()."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);
    }

    public function getProductTypes() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->helper->getUrlProduct(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$this->helper->getApiKey()."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);
    }

    public function ctrl()
    {
        $data = $this->coreSession->getData('ctrl');
    }
}