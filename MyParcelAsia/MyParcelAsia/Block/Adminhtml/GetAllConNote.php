<?php
namespace MyParcelAsia\MyParcelAsia\Block\Adminhtml;
 
class GetAllConNote extends \Magento\Framework\View\Element\Template
{
	public function __construct(
        \MyParcelAsia\MyParcelAsia\Helper\Data $helper,
        \Magento\Framework\View\Element\Template\Context $context
    )
    {
        $this->helper = $helper;
        parent::__construct($context);
    }

    public function getAllConNote() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->helper->getUrlConnote(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$this->helper->getApiKey()."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);
    }
}