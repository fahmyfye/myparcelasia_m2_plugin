<?php
namespace MyParcelAsia\MyParcelAsia\Block\Adminhtml;

use MyParcelAsia\MyParcelAsia\Helper\Data;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\View\Element\Template;
use Magento\Sales\Model\Order;
use Magento\Framework\App\Response\Http;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Session\SessionManagerInterface;

// use Magento\Customer\Model\Session;

 
class GetQuotes extends Template
{
	public function __construct(
        Context $context, 
        Data $helper, 
        Order $orderRepository, 
        Http $response, 
        StoreManagerInterface $storeManager, 
        FormKey $formKey,
        SessionManagerInterface $coreSession,
        array $data = []

    )
    {
        $this->helper = $helper;
        $this->orderRepository = $orderRepository;
        $this->response = $response;
        $this->_storeManager = $storeManager;
        $this->formKey = $formKey;
        $this->coreSession = $coreSession;
        parent::__construct($context, $data);
    }

    public function getBaseUrl()
    {
        return $this->_storeManager->getStore()->getBaseUrl().'admin/myparcelasia/getquotes';
    }

    /**
     * get form key
     *
     * @return string
     */
    public function getFormKey()
    {
         return $this->formKey->getFormKey();
    }

    public function getQuotesDetails() {
        $data = $this->coreSession->getData('mpa_price');
        $data['api_key'] = $this->helper->getApiKey();
        $data['url'] = $this->helper->getUrlQuotes();

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $data['url'],
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$data['api_key']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"scope\"\r\n\r\n".$data['scope']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"total_weight\"\r\n\r\n".$data['total_weight']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"from_postcode\"\r\n\r\n".$data['from_postcode']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to_postcode\"\r\n\r\n".$data['to_postcode']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to_country_id\"\r\n\r\n".$data['country_id']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));

        $result = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        $json = json_decode($result, true);
        $json['increment_id'] = $data['increment_id'];

        return $json;
    }

    public function getCountry() {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->helper->getUrlCountry(),
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$this->helper->getApiKey()."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
            CURLOPT_HTTPHEADER => array(
                "Cache-Control: no-cache",
                "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
            ),
        ));

        $result = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($result, true);
    }

    public function getPostcode()
    {
        return $this->helper->getSenderPostcode();
    }
}