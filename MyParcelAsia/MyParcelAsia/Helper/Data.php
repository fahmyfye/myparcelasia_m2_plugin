<?php

namespace MyParcelAsia\MyParcelAsia\Helper;


use Magento\Framework\App\Helper\AbstractHelper;


class Data extends AbstractHelper
{
    /**
     * MyParcelAsia Setup
     * @param [required] [MPA_API_KEY] [api key]
     * @param [required] [MPA_API_SECRET] [secret key]
     * @param [required] [MPA_PRODUCT_TYPE] [get_product_types]
     * @param [required] [MPA_GET_QUOTE] [get_quotes]
     * @param [required] [MPA_GET_COUNTRY] [get_countries]
     * @param [required] [MPA_CREATE_SHIPMENT] [create_shipment]
     * @param [required] [MPA_CHECKOUT] [checkout]
     * @param [required] [MPA_CON_NOTE] [get_all_connote]
     */
    
    const MPA_API_KEY                               = ""; // You can get this from myparcelasia site
    const MPA_API_SECRET                            = ""; // You can get this from myparcelasia site

    /**
     * Do not edit this section if not needed
     */
    const MPA_PRODUCT_TYPE                          = "https://myparcelasia.com/api/v1/get_product_types";
    const MPA_GET_QUOTE                             = "https://myparcelasia.com/api/v1/get_quotes";
    const MPA_GET_STATES                            = "https://myparcelasia.com/api/v1/get_my_states";
    const MPA_GET_COUNTRY                           = "https://myparcelasia.com/api/v1/get_countries";
    const MPA_CREATE_SHIPMENT                       = "https://myparcelasia.com/api/v1/create_shipment";
    const MPA_GET_ALL_SHIPMENT                      = "https://myparcelasia.com/api/v1/get_all_shipment";
    const MPA_CHECKOUT                              = "https://myparcelasia.com/api/v1/checkout";
    const MPA_CON_NOTE                              = "https://myparcelasia.com/api/v1/get_all_connote";
    const MPA_GET_CART                              = "https://myparcelasia.com/api/v1/get_cart_content";

    /* ****************************************************************************************************** */

    protected $userKey                              = self::MPA_API_KEY;
    protected $userSecret                           = self::MPA_API_SECRET;

    protected $productUrl                           = self::MPA_PRODUCT_TYPE;
    protected $quotesUrl                            = self::MPA_GET_QUOTE;
    protected $statesUrl                            = self::MPA_GET_STATES;
    protected $countryUrl                           = self::MPA_GET_COUNTRY;
    protected $createShipmentUrl                    = self::MPA_CREATE_SHIPMENT;
    protected $allShipmentUrl                       = self::MPA_GET_ALL_SHIPMENT;
    protected $checkoutUrl                          = self::MPA_CHECKOUT;
    protected $connoteUrl                           = self::MPA_CON_NOTE;
    protected $getCartUrl                           = self::MPA_GET_CART;

    protected $userPassword;

    /* ************************************************************************************************************* */

    /**
     * Compony Setup - This info is required
     * @param [string] [required] [MPA_SENDER_NAME] [Sender's/Company's Name]
     * @param [email] [required] [MPA_SENDER_EMAIL] [Sender's/Company's Email Address]
     * @param [number] [required] [MPA_SENDER_CONTACT_1] [Sender's/Company's Contact Number]
     * @param [string] [required] [MPA_SENDER_COMPANY] [Sender's/Company's Name]
     * @param [string] [required] [MPA_SENDER_ADDRESS_1] [Sender's/Company's Address Line 1]
     * @param [string] [required] [MPA_SENDER_ADDRESS_2] [Sender's/Company's Address Line 2]
     * @param [string] [optional] [MPA_SENDER_ADDRESS_3] [Sender's/Company's Address Line 3]
     * @param [string] [optional] [MPA_SENDER_ADDRESS_4] [Sender's/Company's Address Line 4]
     * @param [string] [required] [MPA_SENDER_CITY] [Sender's/Company's City Name]
     * @param [number] [required] [MPA_SENDER_POSTCODE] [Sender's/Company's Postcode]
     * @param [string] [required] [MPA_SENDER_STATE] [Sender's/Company's State]
     * @param [string] [required] [MPA_SENDER_COUNTRY] [Sender's/Company's Country]
     */
    
    const MPA_SENDER_NAME                           = ""; 
    const MPA_SENDER_EMAIL                          = "";

    const MPA_SENDER_CONTACT_1                      = "";
    const MPA_SENDER_COMPANY                        = "";
    const MPA_SENDER_ADDRESS_1                      = "";
    const MPA_SENDER_ADDRESS_2                      = "";
    const MPA_SENDER_ADDRESS_3                      = "";
    const MPA_SENDER_ADDRESS_4                      = "";
    const MPA_SENDER_CITY                           = "";
    const MPA_SENDER_POSTCODE                       = "";
    const MPA_SENDER_STATE                          = "";
    const MPA_SENDER_COUNTRY                        = "";

    protected $senderName                           = self::MPA_SENDER_NAME;
    protected $senderEmail                          = self::MPA_SENDER_EMAIL;

    protected $senderContact1                       = self::MPA_SENDER_CONTACT_1;
    protected $senderCompany                        = self::MPA_SENDER_COMPANY;
    protected $senderAddress1                       = self::MPA_SENDER_ADDRESS_1;
    protected $senderAddress2                       = self::MPA_SENDER_ADDRESS_2;
    protected $senderAddress3                       = self::MPA_SENDER_ADDRESS_3;
    protected $senderAddress4                       = self::MPA_SENDER_ADDRESS_4;
    protected $senderCity                           = self::MPA_SENDER_CITY;
    protected $senderPostcode                       = self::MPA_SENDER_POSTCODE;
    protected $senderState                          = self::MPA_SENDER_STATE;
    protected $senderCountry                        = self::MPA_SENDER_COUNTRY;

    /* ************************************************************************************************************* */
    
    public function __construct()
    {
    }

    /**
     * Get MyParcelAsia Details
     */

    public function getHashPassword() {
        $userPassword   = md5($this->userSecret);
        return $userPassword;
    }

    public function getApiKey()
    {
        return $this->userKey;
    }

    public function getUrlProduct()
    {
        return $this->productUrl;
    }

    public function getUrlQuotes()
    {
        return $this->quotesUrl;
    }

    public function getUrlStates()
    {
        return $this->statesUrl;
    }

    public function getUrlCountry()
    {
        return $this->countryUrl;
    }

    public function getUrlCreateShipment()
    {
        return $this->createShipmentUrl;
    }

    public function getUrlAllShipment()
    {
        return $this->allShipmentUrl;
    }

    public function getUrlCheckout()
    {
        return $this->checkoutUrl;
    }

    public function getUrlConnote()
    {
        return $this->connoteUrl;
    }

    public function getUrlCart()
    {
        return $this->getCartUrl;
    }

    /* *********************************************** */
    /**
     * Get Company's Details
     */
    
    public function getSenderName()
    {
        return $this->senderName;
    }
    
    public function getSenderEmail()
    {
        return $this->senderEmail;
    }
    
    public function getSenderContact1()
    {
        return $this->senderContact1;
    }
    
    public function getSenderCompany()
    {
        return $this->senderCompany;
    }
    
    public function getSenderAddress1()
    {
        return $this->senderAddress1;
    }
    
    public function getSenderAddress2()
    {
        return $this->senderAddress2;
    }
    
    public function getSenderAddress3()
    {
        return $this->senderAddress3;
    }
    
    public function getSenderAddress4()
    {
        return $this->senderAddress4;
    }
    
    public function getSenderCity()
    {
        return $this->senderCity;
    }
    
    public function getSenderPostcode()
    {
        return $this->senderPostcode;
    }
    
    public function getSenderState()
    {
        return $this->senderState;
    }
    
    public function getSenderCountry()
    {
        return $this->senderCountry;
    }
}
