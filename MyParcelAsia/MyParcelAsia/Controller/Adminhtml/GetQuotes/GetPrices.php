<?php
namespace MyParcelAsia\MyParcelAsia\Controller\Adminhtml\GetQuotes;

class GetPrices extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;
    protected $orderRepository;
    protected $searchCriteriaBuilder;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Sales\Model\OrderRepository $orderRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->coreSession = $coreSession;
        $this->orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        parent::__construct($context);
    }

    /**
    * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
    *
    * @return \Magento\Framework\View\Result\Page
    */
    public function execute()
    {
        // Get $_POST data
        $data = $this->getRequest()->getPostValue();
        
        // Check if $_POST data exist
        if(!$data)
        {
            // If $_POST data doesn't exist, create get quote page
            return  $resultPage = $this->resultPageFactory->create();

        } else {
            $this->coreSession->setData('mpa_createshipping', $data);

            return $this->resultRedirectFactory->create()->setPath('myparcelasia/getquotes/createshipping/');
        }
    }
}
?>
