<?php
namespace MyParcelAsia\MyParcelAsia\Controller\Adminhtml\GetQuotes;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
    ) {
        $this->resultPageFactory = $resultPageFactory;
        $this->_coreSession = $coreSession;
        parent::__construct($context);
    }

    /**
    * Load the page defined in view/adminhtml/layout/exampleadminnewpage_helloworld_index.xml
    *
    * @return \Magento\Framework\View\Result\Page
    */
    public function execute()
    {
        // Get $_POST data
        $data = $this->getRequest()->getPostValue();

        // Check if $_POST data exist
        if(!$data)
        {
            // If $_POST data doesn't exist, create get quote page
            return  $resultPage = $this->resultPageFactory->create();

        } else {
            $this->_coreSession->setData('mpa_price', $data);            
            return $this->resultRedirectFactory->create()->setPath('myparcelasia/getquotes/getprices/');
        }
    }
}
?>
