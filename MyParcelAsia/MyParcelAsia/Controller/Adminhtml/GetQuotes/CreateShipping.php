<?php
namespace MyParcelAsia\MyParcelAsia\Controller\Adminhtml\GetQuotes;

class CreateShipping extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \MyParcelAsia\MyParcelAsia\Helper\Data $helper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Session\SessionManagerInterface $coreSession,
        \Magento\Framework\Data\Form\FormKey $formKey
    ) {
        $this->helper = $helper;
        $this->messageManager = $messageManager;
        $this->resultPageFactory = $resultPageFactory;
        $this->coreSession = $coreSession;
        $this->formKey = $formKey;
        parent::__construct($context);
    }

    /**
    * @return \Magento\Framework\View\Result\Page
    */
    public function execute()
    {
        // return  $resultPage = $this->resultPageFactory->create();
        // Get $_POST data
        $data = $this->getRequest()->getPostValue();
        // $data['post']['view'] = $data;
        // Check if $_POST data exist
        if(!$data)
        {
            // If $_POST data doesn't exist, create get quote page
            return  $resultPage = $this->resultPageFactory->create();

        } else {
            $ship = $this->createShipment($data);
            if($ship['data']['shipment_id'] != null)
            {
                $this->messageManager->addSuccess(__("Order has been processed. Some shipment may not gone through. Please check returned shipment for successfully processed shipments only. <br>
                    Your current balance is <strong>RM".$ship['meta']['topup_balance'].".</strong><br>
                    Your Shipment ID is <strong>".$ship['data']['shipment_id']."</strong>
                    <span>
                      <form class='form' id='custom-form' method='post' autocomplete='off' action='' novalidate='novalidate'>
                        <div>
                          <input name='form_key' type='hidden' value='".$this->getFormKey()."'>
                          <input name='shipment_id' type='hidden' value='".$ship['data']['shipment_id']."'>
                          <button type='submit' name='submit_checkout' class='action submit primary' title='Checkout'>
                            <span>Checkout</span>
                          </button>
                        </div>
                      </form>
                    <span>
                    "
                ));
            } else {
                $this->messageManager->addError(__("Order failed to processed. Please check your balance. Your current balance is <strong>RM".$checkout['meta']['topup_balance'].".</strong>"));
            }
            return $this->resultRedirectFactory->create()->setPath('myparcelasia/getallshipment/');
        }
    }

    public function createShipment($data)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->helper->getUrlCreateShipment(),
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$this->helper->getApiKey()."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"scope\"\r\n\r\n".$data['scope']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"from_postcode\"\r\n\r\n".$data['from_postcode']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to_postcode\"\r\n\r\n".$data['to_postcode']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to_state_name\"\r\n\r\n".$data['to_state_name']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"to_country_id\"\r\n\r\n".$data['to_country_id']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"total_weight\"\r\n\r\n".$data['total_weight']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"referrence_id\"\r\n\r\nnull\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"referrence_number\"\r\n\r\n".$data['referrence_number']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"referrence_data\"\r\n\r\nnull\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"send_type\"\r\n\r\n".$data['send_type']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"pickup_date\"\r\n\r\n".$data['pickup_date']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"price_id\"\r\n\r\n".$data['price_id']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_name\"\r\n\r\n".$data['receiver_name']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_phone\"\r\n\r\n".$data['receiver_phone']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_email\"\r\n\r\n".$data['receiver_email']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_address_line_1\"\r\n\r\n".$data['receiver_address_line_1']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_address_line_2\"\r\n\r\n".$data['receiver_address_line_2']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_address_line_3\"\r\n\r\nnull\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_address_line_4\"\r\n\r\nnull\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_company_name\"\r\n\r\nnull\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"receiver_city\"\r\n\r\n".$data['receiver_city']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"content_type_id\"\r\n\r\n".$data['content_type_id']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"content_value\"\r\n\r\n".$data['content_value']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"content_description\"\r\n\r\n".$data['content_description']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"hs_code\"\r\n\r\nnull\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"parcel_length\"\r\n\r\n".$data['parcel_length']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"parcel_width\"\r\n\r\n".$data['parcel_width']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"parcel_height\"\r\n\r\n".$data['parcel_height']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sender_name\"\r\n\r\n".$data['sender_name']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sender_phone\"\r\n\r\n".$data['sender_phone']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sender_email\"\r\n\r\n".$data['sender_email']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sender_address_line_1\"\r\n\r\n".$data['sender_address_line_1']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sender_address_line_2\"\r\n\r\n".$data['sender_address_line_2']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sender_address_line_3\"\r\n\r\nnull\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sender_address_line_4\"\r\n\r\nnull\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"sender_city\"\r\n\r\n".$data['sender_city']."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
          CURLOPT_HTTPHEADER => array(
            "Postman-Token: 41823cde-6bd9-4fcd-b5a9-79bb93ee8fe9",
            "cache-control: no-cache",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);
    }

    /**
     * get form key
     *
     * @return string
     */
    public function getFormKey()
    {
         return $this->formKey->getFormKey();
    }
}
?>
