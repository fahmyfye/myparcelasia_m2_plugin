<?php
namespace MyParcelAsia\MyParcelAsia\Controller\Adminhtml\GetAllShipment;

class Index extends \Magento\Backend\App\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \MyParcelAsia\MyParcelAsia\Helper\Data $helper,
        \Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Session\SessionManagerInterface $coreSession
    ) {
        $this->helper = $helper;
        $this->messageManager = $messageManager;
        $this->resultPageFactory = $resultPageFactory;
        $this->coreSession = $coreSession;
        parent::__construct($context);
    }

    /**
     * @return \Magento\Framework\View\Result\Page
     */
    public function execute()
    {
        // Get $_POST data
        $data = $this->getRequest()->getPostValue();

        // Check if $_POST data exist
        if(!$data)
        {
            $data = $this->coreSession->getData('mpa_shipmentstatus');
            
            // If $_POST data doesn't exist, create get quote page
            return  $resultPage = $this->resultPageFactory->create();

        } else {
            $checkout = $this->getCheckOut($data['shipment_id']);

            if($checkout['data']['order']['id'] != null)
            {
                $this->messageManager->addSuccess(__
                  (
                    "Order has been processed. Some shipment may not gone through. Please check returned shipment for successfully processed shipments only.
                    <br>
                    Your current balance is <strong>RM".$checkout['meta']['topup_balance'].".</strong>
                    <br>
                    <span>
                      <button type='submit' name='download_note' class='action submit primary' title='Download Consignment Note'>
                        <span>
                          <a href=".$checkout['data']['shipments'][0]['consignment_note']." target='_blank' style='text-decoration: none !important;'>
                          Download Consignment Note</a>
                        </span>
                      </button>
                    </span>
                    "
                  )
                );
            } else {
                $this->messageManager->addError(__("Order failed to processed. Please check your balance. Your current balance is <strong>RM".$checkout['meta']['topup_balance'].".</strong>"));
            }
            return  $resultPage = $this->resultPageFactory->create();
            
        }
    }

    public function getCheckOut($shipment_id)
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $this->helper->getUrlCheckout(),
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => "------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"api_key\"\r\n\r\n".$this->helper->getApiKey()."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW\r\nContent-Disposition: form-data; name=\"shipment_id\"\r\n\r\n".$shipment_id."\r\n------WebKitFormBoundary7MA4YWxkTrZu0gW--",
          CURLOPT_HTTPHEADER => array(
            "Postman-Token: 41823cde-6bd9-4fcd-b5a9-79bb93ee8fe9",
            "cache-control: no-cache",
            "content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW"
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        return json_decode($response, true);
    }
}
?>
