MyParcelAsia Magento 2 Plugin Setup

1. Copy the file into:
    i. magento 2 folder > app > code > ‘paste here’

2. Go to:
    i. MyParcelAsia > MyParcelAsia > Helper > Data.php
    ii. Fill in the required fields:
        a) MyParcelAsia Setups:
        b) MPA_API_KEY
        c) MPA_API_SECRET

    iii. Company’s Setup:
        a) MPA_SENDER_NAME
        b) MPA_SENDER_EMAIL
        c) MPA_SENDER_CONTACT_1
        d) MPA_SENDER_COMPANY
        e) MPA_SENDER_ADDRESS_1
        f) MPA_SENDER_ADDRESS_2
        g) MPA_SENDER_ADDRESS_3 (optional)
        h) MPA_SENDER_ADDRESS_4 (optional)
        i) MPA_SENDER_CITY
        j) MPA_SENDER_POSTCODE
        k) MPA_SENDER_STATE
        l) MPA_SENDER_COUNTRY